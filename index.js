import axios from "axios";

const urls = process.env.WEBHOOK_URL?.split(";") ?? [];

if (!urls.length) {
  console.error("No url was found");
  process.exit(1);
}

const promises = urls
  .map((url) => url.trim())
  .map((url) =>
    axios
      .post(url, {})
      .then(() => `Success: ${url}`)
      .catch((e) => `Error: ${url} (${e.message})`)
  );

Promise.all(promises).then((result) => {
  console.log(result.join(`\r\n`));
  const resultCode = result.some((v) => v.startsWith("Error")) ? 1 : 0;
  process.exit(resultCode);
});
